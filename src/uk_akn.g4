grammar uk_akn;


/* 
	Parser for plain text UK and Scottish legislation
	Targets conversion to Akoma Ntoso XML
	
	Developed for use with the Lawmaker application
	Author: Matt Lynch (Based on initial work by Jim Mangiafico for The National Archives)
	
	Type of legislation covered:
		* UK Bills and Acts
		* Scottish Bills and Acts
		* Parliamentary amendments to UK and Scottish Bills
		* Statutory instruments and Scottish Statutory Instruments
		
	Parses "body" text including schedules. Doesn't cover front/back page, table of contents.  
	
	Expects line breaks only at the end of paragraphs of text (i.e. doesn't like text copied and pasted from PDFs) 
*/

/*
	PARSER GRAMMAR ROOT RULES
	*	One of the following rules needs to be invoked in passing text to the parser
		* billParse
		* siParse
		* scheduleParse
		* amendParse
		* miscParse
	*	praseText rule is included as first subrule in each root rule in case parser is passed a simple phrase. The intended result is that a simple node is returned containing the phrase
		rather than the parser attempting to parse it as structured elements (e.g. to avoid "26 apples" being parsed as a section number and heading)
	*	Text that is two or more lines but doesn't match any structured elements will be parsed as multiple simple paragraphs (p rule)
*/

/* 	
	billParse: for text that is either the complete text of a Bill or Act or a portion/provision within the body of the Bill or Act (including text that is a provision within a quoted structure of an amendment)
*/
billParse : phraseText
				|((part+ | chapter+ | section+ | crossHeading+ ) schedules?)
				| schedule+
				| subsection+
				| paragraph+ 
				| subparagraph+ 
				| subsubparagraph+ 
				| definition+ 
				| step+ 
				| p+ ;
	
/*
	siParse: for text that is either the complete text of an SI or a portion/provision within the body of the SI
	*	The article rule will match regulations, rules, articles etc. within an SI/SSI depending on the type since they are formatted/structured the same so further context will be required to generate appropriate XML
*/
siParse : phraseText
				| ((siPart+ | siChapter+ | article+ | siSection | siSubsection ) schedules?)
				| schedule+
				| siParagraph+
				| paragraph+ 
				| subparagraph+ 
				| subsubparagraph+ 
				| definition+ 
				| step+ 
				| p+ ; 

/*
	scheduleParse: for text that is either one or more complete schedules to a Bill, Act or SI or a portion/provision within a schedule (including text that is a provision within a quoted structure of an amendment)
*/
scheduleParse :  phraseText
						| schedule+ 
						| schPart+ 
						| schChapter+ 
						| schCrossHeading+ 
						| scheduleParagraph+ 
						| scheduleSubparagraph+ 
						| paragraph+ 
						| subparagraph+ 
						| subsubparagraph+ 
						| definition+ 
						| step+ 
						| p+ ;
						
/*
	amendParse: for text that either the complete "instruction" of an amendment (i.e. not including the proposer/supporter info) or the quoted structure within an amendment
*/
amendParse :  instructionMod 
					| amendBillQuotedStructure lineEnd 
					| amendScheduleQuotedStructure lineEnd ;

/*
	miscParse: for text that is within the preface/preamble of a Bill or SI or the explanatory note at the end of an SI 
	* TODO: expand to include numbered paragraphs and lists
*/
miscParse : phraseText | p+ ; 

/* GROUPING LEVEL PROVISIONS IN BODY OF BILL */

part: partNumber heading
	( chapter+ 
	| crossHeading+ 
	| section+
	);
	
partNumber: PARTNUM lineEnd;

chapter: chapterNumber heading
	( crossHeading+ 
	| section+
	);
	
chapterNumber: CHAPTERNUM lineEnd;

crossHeading: heading section+ ;

/* SECTION/CLAUSE LEVEL AND SUBSECTIONS IN BILL (prov1 and prov2)*/

section : sectionNumber SPACE? heading 
	( subsection+
	| intro paragraph+ 
	| intro definition+
	| intro paragraph+ wrapUp 
	| intro step+
	| content
	);
	
sectionNumber : NUMBER  ;

subsection : subsectionNumber SPACE?
	(intro paragraph+ 
	| intro paragraph+ wrapUp 
	| intro definition+ 
	| intro step+ 
	| content
	) ;
	
subsectionNumber : SUBSECTIONNUM | EMPTYNUM ;

/* GROUPING LEVEL PROVISIONS IN SI */

siPart: partNumber heading (siChapter+ | siSection | siSubsection | article+ );

siChapter: chapterNumber heading? (siSection | siSubsection | article+ );

siSection: siSectionNumber? heading (siSubsection | article+ );
siSectionNumber : SECTIONNUM lineEnd;

siSubsection: siSubsectionNumber? heading article+ ;
siSubsectionNumber : SISUBSECTIONNUM lineEnd;

/* ARTICLE/REGULATION/RULE LEVEL AND PARAGRAPHS IN SI (prov1 and prov2) */

article : heading? articleNumber PUNCT* SPACE? (siParagraph+ 
											| intro paragraph+ 
											| intro definition+
											| intro paragraph+ wrapUp 
											| content ) ;

articleNumber : NUMBER;

siParagraph : siParagraphNumber SPACE?
	(intro paragraph+
	| intro paragraph+ wrapUp 
	| intro definition+ 
	| paragraph+ 
	| content
	) ;
	
siParagraphNumber : SUBSECTIONNUM | EMPTYNUM ;

siQuotedStructure: startQuote
	( siPart+ 
	| siChapter+ 
	| siSection+ 
	| siSubsection+ 
	| article+
	| siParagraph+
	| paragraph+
	| subparagraph+
	| subsubparagraph+
	| definition+
	| step+ 
	)
	endQuote followingText?  ;


/* SCHEDULES AND GROUPING LEVELS WITHIN SCHEDULES */

schedules: schedulesHeading? schedule+ ;
schedulesHeading: SCHEDULES lineEnd;

schedule : scheduleNumber heading
	(schPart+ 
	| schChapter+ 
	| schCrossHeading+ 
	| scheduleParagraph+ 
	) ;

scheduleNumber: (SCHEDULENUM referenceNote?| referenceNote? SCHEDULENUM ) lineEnd;

referenceNote: lineEnd INTRODUCEDBY | SPACE SECTIONNUM | SECTIONNUM lineEnd;

schPart: partNumber heading (schChapter+ | schCrossHeading+ | scheduleParagraph+ );

schChapter: chapterNumber heading (schCrossHeading+ | scheduleParagraph+) ;

schCrossHeading: heading scheduleParagraph+ ;


/* SCHEDULE PARAGRAPHS AND SUBPARAGRAPHS (schProv1 and schProv2) */

scheduleParagraph: scheduleParagraphNumber SPACE?
	(scheduleSubparagraph+ 
	| intro paragraph+ 
	| intro paragraph+ wrapUp 
	| intro definition+ 
	| intro step+
	| content
	) ;
	
scheduleParagraphNumber: NUMBER;

scheduleSubparagraph: scheduleSubparagraphNumber SPACE?
	(intro paragraph+ 
	| intro paragraph+ wrapUp 
	| intro definition+ 
	| intro step+
	| content
	) ;
																								
scheduleSubparagraphNumber: SUBSECTIONNUM | EMPTYNUM ;
							
/* 
	PARAGRAPHS. SUB-PARAGRAPHS AND OTHER PROVISIONS BELOW PROV2 THAT ARE USED EVERYWHERE
	* including: para1, para2, para3, definition and step
*/

paragraph : paragraphNumber SPACE? 
	(intro subparagraph+ 
	| intro subparagraph+ wrapUp 
	| content
	) ;
	
paragraphNumber : PARAGRAPHNUM  ;

subparagraph : subparagraphNumber SPACE? 
	(intro subsubparagraph+ 
	| intro subsubparagraph+ wrapUp 
	| content
	) ;
	
subparagraphNumber : SUBPARAGRAPHNUM ;

subsubparagraph : subsubparagraphNumber SPACE? content ;
subsubparagraphNumber : SUBSUBPARAGRAPHNUM ;

definition: definitionIntro paragraph+
			| definitionIntro paragraph+ wrapUp 
			| definitionContent ;

step: stepNumber
	(content
	| intro paragraph+ 
	| intro paragraph+ wrapUp 
	) ;
	
stepNumber: STEPNUM lineEnd ;

/* CONSTITUENT ELEMENTS OF ALL PROVISIONS */

heading: text lineEnd ;

intro : p lineEnd ;

definitionIntro: definitionP lineEnd ;

wrapUp: p ;

/* Detection of lists was generating too many false positives so commented out this version of content rule
content : p | pmod | p list;
*/

content : p | pmod | pnestedmod ;

definitionContent: definitionP ;

p : text lineEnd? ; 

definitionP :  definedTerm PUNCT? SPACE text lineEnd?;

definedTerm: startQuote text endQuote ;

pmod : 	text ( INSERTOPENQUOTE lineEnd ( openBillQuotedStructure | scheduleQuotedStructure | siQuotedStructure)
			| lineEnd ( billQuotedStructure | scheduleQuotedStructure | siQuotedStructure)
			)
		followingText? lineEnd;

// pnestedmod  allows for nested quoted structures where there is no line break between end of one and end of other
pnestedmod : text lineEnd ( billQuotedStructure | scheduleQuotedStructure | siQuotedStructure) followingText?;

list : item item+ ;

item : itemNumber? SPACE? p ;

itemNumber: (PUNCT | ANY) ;

/* 
	text rule is basic rule that sweeps up text content of provisions
	* It includes all lexer tokens that could possibly occur within a provision (and in that context we wouldn't want to parse as anything other than text).
*/
text: definedTerm (TEXT | QUOTEDTEXT | REF | NUMBER | PUNCT | SPACE | LT | GT | CONJUNCTION | SCHEDULES | SCHEDULENUM | SECTIONNUM | SUBSECTIONNUM | PARAGRAPHNUM | SUBPARAGRAPHNUM | SUBSUBPARAGRAPHNUM | PARTNUM | CHAPTERNUM | ANY )*
	| (TEXT | QUOTEDTEXT | REF | PUNCT | CONJUNCTION | SCHEDULES | SCHEDULENUM | SECTIONNUM | PARTNUM | CHAPTERNUM | ANY) (TEXT | QUOTEDTEXT | REF | NUMBER | PUNCT | SPACE | LT | GT | CONJUNCTION | SCHEDULES | SCHEDULENUM | SECTIONNUM | SUBSECTIONNUM | PARAGRAPHNUM | SUBPARAGRAPHNUM | SUBSUBPARAGRAPHNUM | PARTNUM | CHAPTERNUM | ANY )*;
		
// phraseText rule matches single line of text. Used to match content that really shouldn't be going through the parser.		
phraseText: (TEXT | QUOTEDTEXT | REF | NUMBER | PUNCT | SPACE | LT | GT | CONJUNCTION | SCHEDULES | SCHEDULENUM | SECTIONNUM | SUBSECTIONNUM | PARAGRAPHNUM | SUBPARAGRAPHNUM | SUBSUBPARAGRAPHNUM | PARTNUM | CHAPTERNUM | ANY )+ lineEnd?;		
lineEnd : EOL ;

/* 
	QUOTED STRUCTURES
	* covers quoted structures in Bills, SIs and parliamentary amendments
	* amendBillQuotedStructure comes before BillQuotedStructure to give it precedence to help with nested quoted structures
	* amendScheduleQuotedStructure comes before BillScheduleStructure to give it precedence to help with nested quoted structures
*/

amendBillQuotedStructure: startQuote?
							(	part+ |
								chapter+ |
								crossHeading+ |
								section+ |
								unnumberedSection+ |
								subsection+ |
								paragraph+ |
								subparagraph+ |
								subsubparagraph+ |
								definition+ |
								step+
							)
							endQuote ;
	
billQuotedStructure: startQuote
							(	part+ |
								chapter+ |
								section+ |
								crossHeading |
								subsection+ |
								paragraph+ |
								subparagraph+ |
								subsubparagraph+ |
								definition+ |
								step+ |
								p
							)
							endQuote ; 
							  
openBillQuotedStructure: 	(	part+ |
								chapter+ |
								section+ |
								crossHeading |
								subsection+ |
								paragraph+ |
								subparagraph+ |
								subsubparagraph+ |
								definition+ |
								step+ 
							)
							endQuote ;  						
						
amendScheduleQuotedStructure: startQuote?
							(	schedule+ |
								schPart+ |
								schChapter+ |
								schCrossHeading+ |
								scheduleParagraph+ |
								scheduleSubparagraph+ 
							)
							endQuote  ;

scheduleQuotedStructure: startQuote
							(	schedule+ |
								schPart+ |
								schChapter+ |
								schCrossHeading |
								scheduleParagraph + |
								scheduleSubparagraph+ 
							)
							endQuote ;
							
startQuote: QUOTE | LT;

endQuote: QUOTE | GT;

followingText: PUNCT SPACE? CONJUNCTION? ;

/* AMENDMENT PROVISIONS */

instructionMod : (text | text (QUOTE NUMBER? text | INSERTOPENQUOTE) ) lineEnd ( amendBillQuotedStructure | amendScheduleQuotedStructure ) lineEnd ;

/* 	sections are usually identified by having a simple arabic number followed by a heading.
	However, in amendments, the number can be left out
	So the unnumberedSection rule caters specifically for an amendment that consists of a single unnumbered section
	Doesn't work if the section is preceded by a cross-heading */

unnumberedSection: unnumberedSectionNumber SPACE? heading 
	( subsection+
	| intro paragraph+ 
	| intro paragraph+ wrapUp 
	| intro definition+
	| intro step+
	| content
	);
	
unnumberedSectionNumber : ;


/* LEXER TOKENS */

QUOTE: '"' | '“' | '”' | '\u201C' | '\u201D';

EOL : '\r'? '\n' (SPACE | '\r'? '\n')* ;

SPACE : (' ' | '\t')+ ;

fragment PART : 'PART' | 'Part' ;
PARTNUM : PART SPACE NUMBER UPPER?;
fragment CHAPTER : 'CHAPTER' | 'Chapter' ;
CHAPTERNUM: CHAPTER SPACE NUMBER UPPER? ;
fragment SECTION : 'SECTION' | 'Section' | 'section' ;
SECTIONNUM: SECTION SPACE NUMBER UPPER?;
fragment SISUBSECTION : 'SUB-SECTION' | 'Sub-Section' | 'Sub-section' | 'Subsection' | 'SUBSECTION' ;
SISUBSECTIONNUM: SISUBSECTION SPACE NUMBER UPPER?;
SCHEDULES : 'SCHEDULES' | 'Schedules' ;
fragment SCHEDULE : 'SCHEDULE' | 'Schedule' ;
SCHEDULENUM : SCHEDULE SPACE NUMBER UPPER? | SCHEDULE ;

INTRODUCEDBY :  ('(introduced by' | '(Introduced by') SPACE SECTIONNUM ')' ;

SUBSECTIONNUM: '(' NUMBER UPPER? ')' ;

SUBPARAGRAPHNUM: '(' ROMAN ')' ;

PARAGRAPHNUM: '(' LOWER ')' ;

SUBSUBPARAGRAPHNUM: '(' UPPER ')' ;

STEPNUM: ( 'Step' | 'step' | 'STEP' ) SPACE ( NUMBER | LOWER | UPPER ) ;

EMPTYNUM: '(  )' | '( )'; // Parliamentary amendments leave the number of some provisions empty (with an ordinary space or figure space between parenthesis)

fragment ROMAN :  LOWER? ('i' | 'v' | 'x')+ LOWER? ; // Numbers can have letters before and after if they have been added by amendment

fragment UPPER : ( 'A'..'Z' )+ ;
fragment LOWER : ( 'a'..'z' )+ ;

fragment WORD: (UPPER | LOWER)+ ;

/* 	NUMBER allows for fairly complex numbers, including those that begin and/or end with capital letters, .e.g, 34B
	Semantic predicate at the end is to stop the first letter of the section heading being incorporated into
	the num if there is no space between them, e.g. num is "24" in "24Interpretation" and 24A in "24AInterpretation" */
	
NUMBER : UPPER? ('0'..'9')+ (UPPER+ ('0'..'9')+)* (UPPER {!(Character.isLowerCase(_input.LA(1)))}?)* ;  /* Numbers can have letters before and after if they have been added by amendment */

PUNCT : '\u2014' | '-' | '–' | '—' | ',' | '.' | ';' | ':' | '!' | '\'' | '\u002F' | '’' | '\u0027' | '\u2019' | '?' | '(' | ')' | '%' | '£' | '$' | '&' | '*' | '•' ;

// less-than and greater-than symbols are used instead of quote marks in Scottish amendments
LT : '<' ;
GT : '>' ;

CONJUNCTION: 'or' | 'and' | 'but';

//semantic predicate to check for space or bracket instead: {self._input.LA(-1) == ord(' ') or self._input.LA(-1) == ord('(')}?

QUOTEDTEXT:  (SPACE | '(' | 'insert ' | 'substitute ') QUOTE QUOTE? (TEXT | CONJUNCTION | SPACE | NUMBER | PUNCT | SUBSECTIONNUM | PARAGRAPHNUM | SUBPARAGRAPHNUM | SUBSUBPARAGRAPHNUM | PARTNUM | CHAPTERNUM | SECTIONNUM | SCHEDULENUM )+ QUOTE QUOTE? ')'?  ;

INSERTOPENQUOTE: ('insert ' | 'substitute ') QUOTE QUOTE? (TEXT | CONJUNCTION | SPACE | NUMBER | PUNCT | SUBSECTIONNUM | PARAGRAPHNUM | SUBPARAGRAPHNUM | SUBSUBPARAGRAPHNUM | PARTNUM | CHAPTERNUM | SECTIONNUM | SCHEDULENUM )* ;

REF: ('section' | 'subsection' | 'paragraph' | 'sub' '-'? 'paragraph' | 'sub' '-'? 'sub' '-'? 'paragraph') 's'? SPACE NUMBER? (SPACE? ( SUBSECTIONNUM | PARAGRAPHNUM | SUBPARAGRAPHNUM | SUBSUBPARAGRAPHNUM)+ PUNCT?)+ ((CONJUNCTION | 'to') SPACE NUMBER? ( SUBSECTIONNUM | PARAGRAPHNUM | SUBPARAGRAPHNUM | SUBSUBPARAGRAPHNUM)+)?;

// TEXT: (UPPER|LOWER) ( WORD | PUNCT | SPACE | NUMBER)* ;

TEXT: (UPPER|LOWER) ( WORD | NUMBER )* ;

ANY: .; 
